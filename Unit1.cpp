//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

class TArticle : public TObject {
  private:
    String FName;
    float FPrice;
    int FStock;
  public:
    __property String Name = { read = FName, write = FName };
    __property float Price = { read = FPrice, write = FPrice };
    __property int Stock = { read = FStock, write = FStock };
};

String ArticlesFile;
String DBMode; // "B" for Browse, "A" for Add, "E" for Edit

TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
  Articles = new TStringList;
  Articles->CaseSensitive = false;
  ArticlesFile = ExtractFilePath(Application->ExeName) + "articles.txt";
  LoadArticles();
  Articles->Sorted = true;
  // the stringlist's objects must be automatically freed upon...
  // ...destroying the stringlist
  Articles->OwnsObjects = true;

  stStatus->Caption = "";
  edFind->Text = "";
  UpdateUI("");
  Grid->ColWidths[0] = 60;
  Grid->ColWidths[1] = 120;
  Grid->ColWidths[2] = 80;
  Grid->ColWidths[3] = 60;
  Grid->Cells[0][0] = "ID";
  Grid->Cells[1][0] = "Name";
  Grid->Cells[2][0] = "Price";
  Grid->Cells[3][0] = "Stock";
}
//----------------------------------------------------------------------------
void TForm1::LoadArticles()
{
  int index, posit;
  String S, ID;
  TStringList *SL;
  SL = new TStringList;
  if (!FileExists(ArticlesFile))
    ShowMessage("Not found: " + ArticlesFile);
  else {
    SL->LoadFromFile(ArticlesFile);
    for (index = 0; index < SL->Count; index++) {
      TArticle *Art = new TArticle;
      S = SL->Strings[index];
      posit = S.Pos(";");
      ID = S.SubString(1, posit - 1);
      S.Delete(1, posit);
      posit = S.Pos(";");
      Art->Name = S.SubString(1, posit - 1);
      S.Delete(1, posit);
      posit = S.Pos(";");
      Art->Price = StrToFloat(S.SubString(1, posit - 1));
      S.Delete(1, posit);
      Art->Stock = StrToInt(S);
      Articles->AddObject(ID, Art);
    }
    UpdateGrid(0);
  }
  delete SL;
}
//----------------------------------------------------------------------------
void TForm1::SaveArticles()
{
  int index;
  String S;
  TStringList *SL;
  SL = new TStringList;
  for (index = 0; index < Articles->Count; index++) {
    TArticle *Art = static_cast<TArticle*>(Articles->Objects[index]);
    S = Articles->Strings[index] + ";";
    S = S + Art->Name + ";";
    S = S + FormatFloat("0.00", Art->Price) + ";";
    S = S + IntToStr(Art->Stock);
    SL->Add(S);
  }
  SL->SaveToFile(ArticlesFile);
  delete SL;
}
//----------------------------------------------------------------------------
void TForm1::UpdateUI(String Stat, String DBModeNew)
{
  stStatus->Caption = Stat;
  DBMode = DBModeNew;
  Panel1->Visible = ((DBMode == "A") || (DBMode == "E"));
  // disable buttons when Panel1 is visible
  btnAdd->Enabled = !(Panel1->Visible);
  btnEdit->Enabled = !(Panel1->Visible);
  btnDelete->Enabled = !(Panel1->Visible);
  btnFindID->Enabled = !(Panel1->Visible);
  btnFindName->Enabled = !(Panel1->Visible);
  edFind->Enabled = !(Panel1->Visible);
  if (DBMode == "A") {  // clear edit boxes
    edID->Text = "";
    edName->Text = "";
    edPrice->Text = "";
    edStock->Text = "";
  }
  if ((DBMode == "A") || (DBMode == "E"))
    edID->SetFocus();
}
//----------------------------------------------------------------------------
void TForm1::UpdateGrid(int RowNo)
{
  int index;
  Grid->RowCount = Articles->Count + 1;
  for (index = 0; index < Articles->Count; index++)
  {
    TArticle *Art = static_cast<TArticle*>(Articles->Objects[index]);
    Grid->Cells[0][index + 1] = Articles->Strings[index];
    Grid->Cells[1][index + 1] = Art->Name;
    Grid->Cells[2][index + 1] = FormatFloat("0.00", Art->Price);
    Grid->Cells[3][index + 1] = IntToStr(Art->Stock);
  }
  if (RowNo > (Grid->RowCount - 1))
    RowNo = 0;
  Grid->Row = RowNo;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnAddClick(TObject *Sender)
{
  UpdateUI(" Adding article ", "A");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnEditClick(TObject *Sender)
{
  int index;
  if (Grid->Row < 1)
    stStatus->Caption = " Select article to be edited ";
  else {
    UpdateUI(" Editing article ", "E");
    index = Grid->Row - 1;
    TArticle *Art = static_cast<TArticle*>(Articles->Objects[index]);
    edID->Text = Articles->Strings[index];
    edName->Text = Art->Name;
    edPrice->Text = FormatFloat("0.00", Art->Price);
    edStock->Text = IntToStr(Art->Stock);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnDeleteClick(TObject *Sender)
{
  if (Grid->Row < 1)
    stStatus->Caption = " Select article to be deleted ";
  else {
    if (MessageDlg("Really delete this article?",
                   mtConfirmation, mbYesNoCancel, 0) == mrYes) {
      Articles->Delete(Grid->Row - 1);
      UpdateGrid(1);
      stStatus->Caption = " Article was deleted ";
    }
    else
      stStatus->Caption = " Article was not deleted ";
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnOKClick(TObject *Sender)
{
  int index;
  String ID;
  ID = edID->Text;
  TArticle *Art = new TArticle;
  if ((DBMode == "A") && (Articles->Find(ID, index)))
    UpdateUI(" " + ID + " already exists ", "A");
  else {
    Art->Name = edName->Text;
    Art->Price = StrToFloat(edPrice->Text);
    Art->Stock = StrToInt(edStock->Text);
    if (DBMode == "A") {
      Articles->AddObject(ID, Art);
      Articles->Find(ID, index);
      UpdateUI(" Article was added ");
    }
    else {
      index = Grid->Row - 1;
      Articles->Sorted = false; // next line not allowed for sorted list
      Articles->Strings[index] = ID;
      Articles->Objects[index] = Art;
      Articles->Sorted = true;
      UpdateUI(" Article was modified ");
    }
    UpdateGrid(index + 1);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnCancelClick(TObject *Sender)
{
  UpdateUI(" No article added or modified ");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnFindIDClick(TObject *Sender)
{
  int index;
  if (Articles->Find(edFind->Text, index)) {
    UpdateUI(" Article was found ");
    Grid->Row = index + 1;
    Grid->SetFocus();
  }
  else
    UpdateUI(" ID not found: " + edFind->Text + " ");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnFindNameClick(TObject *Sender)
{
  int index, posit;
  int foundwhere = -1;
  String Name;
  String findwhat = UpperCase(edFind->Text); // Find case-insensitive
  for (index = 0; index < Articles->Count; index++)
  {
    TArticle *Art = static_cast<TArticle*>(Articles->Objects[index]);
    Name = UpperCase(Art->Name); // Find case-insensitive
    posit = Name.Pos(findwhat);
    if (posit == 1) {
      foundwhere = index;
      break;  // if found, break out of the loop
    }
  }
  if (foundwhere > -1) {
    UpdateUI(" Article was found ");
    Grid->Row = foundwhere + 1;
    Grid->SetFocus();
  }
  else
    UpdateUI(" NAME not found: " + edFind->Text + " ");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormDestroy(TObject *Sender)
{
  SaveArticles();
  delete Articles;
}
//---------------------------------------------------------------------------

