object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Articles -- part 4'
  ClientHeight = 390
  ClientWidth = 553
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ID: TLabel
    Left = 9
    Top = 101
    Width = 90
    Height = 13
    Caption = 'ID or part of Name'
  end
  object btnAdd: TButton
    Left = 8
    Top = 8
    Width = 92
    Height = 25
    Caption = 'Add'
    TabOrder = 0
    OnClick = btnAddClick
  end
  object btnFindID: TButton
    Left = 8
    Top = 142
    Width = 92
    Height = 25
    Caption = 'Find ID'
    TabOrder = 1
    OnClick = btnFindIDClick
  end
  object Panel1: TPanel
    Left = 110
    Top = 31
    Width = 328
    Height = 78
    TabOrder = 2
    object Label6: TLabel
      Left = 79
      Top = 2
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object Label2: TLabel
      Left = 207
      Top = 2
      Width = 23
      Height = 13
      Caption = 'Price'
    end
    object Label4: TLabel
      Left = 277
      Top = 3
      Width = 26
      Height = 13
      Caption = 'Stock'
    end
    object Label1: TLabel
      Left = 4
      Top = 3
      Width = 11
      Height = 13
      Caption = 'ID'
    end
    object btnOK: TBitBtn
      Left = 82
      Top = 45
      Width = 75
      Height = 25
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 4
      OnClick = btnOKClick
    end
    object btnCancel: TBitBtn
      Left = 169
      Top = 46
      Width = 74
      Height = 25
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 5
      OnClick = btnCancelClick
    end
    object edName: TEdit
      Left = 80
      Top = 17
      Width = 118
      Height = 21
      TabOrder = 1
      Text = 'edName'
    end
    object edPrice: TEdit
      Left = 207
      Top = 17
      Width = 52
      Height = 21
      TabOrder = 2
      Text = 'edPrice'
    end
    object edStock: TEdit
      Left = 265
      Top = 17
      Width = 52
      Height = 21
      TabOrder = 3
      Text = 'edStock'
    end
    object edID: TEdit
      Left = 4
      Top = 17
      Width = 69
      Height = 21
      TabOrder = 0
      Text = 'edID'
    end
  end
  object stStatus: TStaticText
    Left = 110
    Top = 8
    Width = 44
    Height = 17
    BorderStyle = sbsSingle
    Caption = 'stStatus'
    TabOrder = 3
  end
  object edFind: TEdit
    Left = 8
    Top = 117
    Width = 90
    Height = 21
    TabOrder = 4
    Text = 'edFind'
  end
  object Grid: TStringGrid
    Left = 106
    Top = 115
    Width = 430
    Height = 264
    ColCount = 4
    DefaultColWidth = 60
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    ScrollBars = ssVertical
    TabOrder = 5
  end
  object btnFindName: TButton
    Left = 8
    Top = 170
    Width = 92
    Height = 25
    Caption = 'Find Name'
    TabOrder = 6
    OnClick = btnFindNameClick
  end
  object btnEdit: TButton
    Left = 8
    Top = 36
    Width = 92
    Height = 25
    Caption = 'Edit'
    TabOrder = 7
    OnClick = btnEditClick
  end
  object btnDelete: TButton
    Left = 8
    Top = 63
    Width = 92
    Height = 25
    Caption = 'Delete'
    TabOrder = 8
    OnClick = btnDeleteClick
  end
end
