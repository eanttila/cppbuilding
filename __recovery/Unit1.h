//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
  TButton *btnAdd;
  TButton *btnFindID;
  TPanel *Panel1;
  TBitBtn *btnOK;
  TBitBtn *btnCancel;
  TLabel *Label6;
  TLabel *Label2;
  TLabel *Label4;
  TEdit *edName;
  TEdit *edPrice;
  TEdit *edStock;
  TLabel *Label1;
  TEdit *edID;
  TStaticText *stStatus;
  TEdit *edFind;
  TStringGrid *Grid;
  TButton *btnFindName;
  TButton *btnEdit;
  TButton *btnDelete;
  TLabel *ID;
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall btnAddClick(TObject *Sender);
  void __fastcall btnOKClick(TObject *Sender);
  void __fastcall btnFindIDClick(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
  void __fastcall btnCancelClick(TObject *Sender);
  void __fastcall btnFindNameClick(TObject *Sender);
  void __fastcall btnEditClick(TObject *Sender);
  void __fastcall btnDeleteClick(TObject *Sender);
private:	// User declarations
  TStringList *Articles;
  void LoadArticles();
  void SaveArticles();
  void UpdateGrid(int RowNo);
  void UpdateUI(String Stat, String DBModeNew = "B");
public:		// User declarations
  __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
